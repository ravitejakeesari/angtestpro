import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedserviceService } from '../sharedservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login_req: any = {}
  email = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required])
  constructor(private router: Router, private sharedServices: SharedserviceService) { }

  ngOnInit(): void {
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'Username should not be blank';
    }
    // return this.email.hasError('email') ? 'Not a valid email' : '';
  }

  getPassword_ErrorMessage() {
    if (this.password.hasError('required')) {
      return 'Password should not be blank';
    }
    return '';
  }

  onClear() {
    this.email.reset();
    this.password.reset();
  }

  onLogin() {
    if (this.email.valid && this.password.valid) {
      this.router.navigate(['/homepage']);
      localStorage.setItem('loginUsername', 'test');
      this.sharedServices.signoutNotification$.next(true)
    }
    else this.formMarkAllTouched();

  }
  formMarkAllTouched() {
    this.email.markAllAsTouched();
    this.password.markAllAsTouched();
  }

}
