import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedserviceService {
  public signoutNotification$: Subject<any> = new Subject();
  public dailogNotification$: Subject<any> = new Subject();
  public dailogDataAfterDismissal$: Subject<any> = new Subject();
  public dailogNotificationOff$: Subject<any> = new Subject();

  constructor() {

  }


}
