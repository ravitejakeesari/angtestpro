import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SharedserviceService } from './sharedservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngTestPro';
  showSignout: boolean = false;

  constructor(private router: Router, private sharedService: SharedserviceService, public dialog: MatDialog) {
    this.sharedService.signoutNotification$.subscribe(data => {
      // if (localStorage.getItem('loginUsername'))
      this.showSignout = true;
      // else this.showSignout = false;
    })
    this.sharedService.dailogNotification$.subscribe(data => {
      this.openDialog(data);
      if (data.type && data.type == 'spinner') {

      }
      // this.showProgressBar = true;
    })
  }

  openDialog(data): void {

    // else {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {

      data: data,
      hasBackdrop: true,
      disableClose: true,
      // panelClass: 'dialogContainer',
    });

    dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
    });
    // }

  }

  onSignout() {
    this.showSignout = false
    localStorage.clear();
    this.router.navigate(['login'])
  }


}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: './dialog-overview-example-dialog.html',
  styleUrls: ['./app.component.css']
})
export class DialogOverviewExampleDialog implements OnInit {
  dailogData: any;
  constructor(private sharedService: SharedserviceService, public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.dailogData = this.data
    this.sharedService.dailogNotificationOff$.subscribe(data => {
      this.dialogRef.close();
    })
  }

  ngOnInit() {

  }

  onDailogActionBtn(btnType): void {
    if (this.dailogData.type && this.dailogData.type == 'alert') {
      if (btnType == 'Dismiss') this.dialogRef.close();

    }
    else if (this.dailogData.type && (this.dailogData.type == 'confirm' || this.dailogData.type == 'change' || this.dailogData.type == 'change1')) {
      if (this.dailogData.type == 'confirm') {
        if (btnType == 'Yes') {
          this.dialogRef.close();
          this.sharedService.dailogDataAfterDismissal$.next('Yes');
        }
        else if (btnType == 'No') {
          this.dialogRef.close();
          this.sharedService.dailogDataAfterDismissal$.next('No');
        }
        else if (btnType == 'Receipt') {
          this.dialogRef.close();
          this.sharedService.dailogDataAfterDismissal$.next('receipt');
        }
        else if (btnType == 'Invoice') {
          this.dialogRef.close();
          this.sharedService.dailogDataAfterDismissal$.next('invoice');
        }
        else if (btnType == 'Cancel') {
          this.dialogRef.close();
          this.sharedService.dailogDataAfterDismissal$.next('cancel');
        }
        else if (btnType == 'Signout') {
          this.dialogRef.close();
          this.sharedService.dailogDataAfterDismissal$.next('Signout');
        }
      }
    }
    // if (btnType == 'Dismiss')
    //   this.dialogRef.close();
    // else if(){

    // }
  }
}
