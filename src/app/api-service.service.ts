import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  campusDetails = [
    {
      'Campus Name': 'campus One',
      'View Details': 'view Details'
    },
    {
      'Campus Name': 'campus two',
      'View Details': 'view Details'
    },
    {
      'Campus Name': 'campus three',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus four',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus five',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus six',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus seven',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus eight',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus nine',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus ten',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus eleven',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus twelve',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus thirteen',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus fourteen',
      'View Details': 'view Details'
    }, {
      'Campus Name': 'campus fifteen',
      'View Details': 'view Details'
    },
  ]
  operation: boolean = false;
  constructor(private snackBar: MatSnackBar) { }

  displaySnackBar(message) {
    this.snackBar.open(message, 'Dismiss', { duration: 3500, verticalPosition: 'bottom' });
  }
  getCampusDetails() {
    return this.campusDetails;
  }

  createCampusDetails(campusDetails) {
    this.campusDetails.push(campusDetails);
    this.displaySnackBar('Added')
    this.operation = true;
  }

  updateCampusDetails(oldCampusDetails, newCampusDetails) {
    for (let i = 0; i < this.campusDetails.length; i++) {
      if (this.campusDetails[i]['Campus Name'] == oldCampusDetails['Campus Name']) {
        this.campusDetails.splice(i, 1, newCampusDetails)
      }
    }
    // let index = this.campusDetails.findIndex(oldCampusDetails['Campus Name']);
    // this.campusDetails.splice(index, 1, newCampusDetails)
    this.displaySnackBar('Updated')
    this.operation = true;
  }

  deleteCampusDetails(campusDetails) {
    for (let i = 0; i < this.campusDetails.length; i++) {
      if (this.campusDetails[i]['Campus Name'] == campusDetails['Campus Name']) {
        this.campusDetails.splice(i, 1)
      }
    }
    this.displaySnackBar('Deleted')
    this.operation = true;
  }
}
