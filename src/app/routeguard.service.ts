import { Injectable } from '@angular/core';
import { CanActivate, Router } from "@angular/router";
import { SharedserviceService } from './sharedservice.service';
@Injectable({
  providedIn: 'root'
})
export class RouteguardService {
  canActivate(
    route: import("@angular/router").ActivatedRouteSnapshot,
    state: import("@angular/router").RouterStateSnapshot
  ):
    | boolean
    | import("@angular/router").UrlTree
    | import("rxjs").Observable<boolean | import("@angular/router").UrlTree>
    | Promise<boolean | import("@angular/router").UrlTree> {
    if (localStorage.getItem("loginUsername")) {

      // this.sharedServices.displayUsername$.next(true)
      return false;
    }
    else {

      return true;
    }
    throw new Error("Method not implemented.");
  }
  constructor(private sharedServices: SharedserviceService, private router: Router) {

  }
}
