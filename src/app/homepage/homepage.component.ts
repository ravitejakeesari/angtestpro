import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedserviceService } from '../sharedservice.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  opened: boolean = true;
  innerWidth: any;
  showSideNavToggle: boolean = false;

  constructor(private sharedServices: SharedserviceService, private router: Router) {
    this.sharedServices.signoutNotification$.next(true)

  }
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth <= 768) {
      this.opened = false;
      this.showSideNavToggle = true;
    }
    else {
      this.opened = true;
      this.showSideNavToggle = false;
    }

  }
  ngOnInit(): void {
    this.onResize()
  }

  onToggle(val) {
    if (val == 'admin')
      this.router.navigate(['/homepage/admin'])
    else if (val == 'studentdetails')
      this.router.navigate(['/homepage/studentdetails'])
  }

}
