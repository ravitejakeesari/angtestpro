import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentdetailsRoutingModule } from './studentdetails-routing.module';
import { StudentdetailsComponent } from './studentdetails.component';
import { CommonMaterialModule } from 'src/app/shared-modules/common-material/common-material.module';


@NgModule({
  declarations: [StudentdetailsComponent],
  imports: [
    CommonModule,
    StudentdetailsRoutingModule,
    CommonMaterialModule
  ]
})
export class StudentdetailsModule { }
