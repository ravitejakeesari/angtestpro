import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApiServiceService } from 'src/app/api-service.service';
import { SharedserviceService } from 'src/app/sharedservice.service';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  showForm = 'displayData';
  campusName: any = '';
  oldCampusName: any = '';
  campusNameFormCtrl = new FormControl(null, [Validators.required]);
  displayedColumns: string[] = ['Campus Name', 'View Details'];
  ELEMENT_DATA: any = [{
    'Campus Name': 'Campus One',
    'View Details': 'View Details'
  }]
  // fields:any = []
  // displayedColumns:any = fields;
  dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
  private paginator: MatPaginator;

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    if (this.dataSource)
      this.dataSource.paginator = this.paginator;
  }
  constructor(private apiService: ApiServiceService, private sharedService: SharedserviceService) {
    this.ELEMENT_DATA = this.apiService.getCampusDetails();
    this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;

  }

  ngOnInit(): void {
  }

  onViewIcon(event) {
    this.showForm = 'showViewForm';
    this.campusName = event['Campus Name'];
  }

  onUpdateIcon(event) {
    console.log(event);
    this.showForm = 'showUpdateForm';
    this.campusName = event['Campus Name'];
    this.oldCampusName = this.campusName;
  }

  onDeleteIcon(event) {
    console.log(event);
    let dailogMessage = 'Do you want to delete ?'
    let dailogData = {
      type: 'confirm',
      message: dailogMessage,
      okBtnText: 'Yes',
      cancelBtnText: 'No'
    }
    this.sharedService.dailogNotification$.next(dailogData)
    this.sharedService.dailogDataAfterDismissal$.subscribe(res => {
      if (res == 'Yes') {
        this.apiService.deleteCampusDetails(event);
        this.ELEMENT_DATA = this.apiService.getCampusDetails();
        this.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
        this.apiService.operation = false;
      }
    })
    // this.sharedService.progressBarNotification$.next(true)

  }

  onCreate() {
    this.campusName = '';
    this.showForm = 'showCreateForm'
  }

  onAdd() {
    let obj = {
      'Campus Name': this.campusName,
      'View Details': 'viewdetails'
    }
    if (this.campusNameFormCtrl.valid) {
      this.apiService.createCampusDetails(obj)
      if (this.apiService.operation) {
        this.onCancel();
        this.apiService.operation = false;
      }
    }
    else this.campusNameFormCtrl.markAllAsTouched();
  }

  onUpdate() {
    let newObj = {
      'Campus Name': this.campusName,
      'View Details': 'viewdetails'
    }
    let preObj = {
      'Campus Name': this.oldCampusName,
      'View Details': 'viewdetails'
    }
    if (this.campusNameFormCtrl.valid) {
      this.apiService.updateCampusDetails(preObj, newObj);
      if (this.apiService.operation) {
        this.onCancel();
        this.apiService.operation = false;
      }
    }
    else this.campusNameFormCtrl.markAllAsTouched();
  }

  onCancel() {
    this.showForm = 'displayData'
  }

}
